# Lupus Stark

A plain theme with slim templates and without any styling or javascript.

It is s sole purpose is to produce clean markup for Drupal core elements like form elements and blocks, such that the HTML can be easily embedded and styled elsewhere.
For example, it can be used to embed forms within [Lupus Custom Elements](https://www.drupal.org/project/custom_elements) rendered responses.

## Credits

* Initial development by Wolfgang Ziegler, Tavi Toporjinschi - drunomics GmbH <hello@drunomics.com>
